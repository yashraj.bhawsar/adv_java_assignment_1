package com.nagarro.AdvanceJavaAssignment1.controller;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

import com.nagarro.AdvanceJavaAssignment1.input.*;
import com.nagarro.AdvanceJavaAssignment1.model.*;

public class Utility {

    public static final HashMap<String, HashSet<Tshirt>> tshirtsInfo = new HashMap<>();

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<Tshirt> result = new ArrayList<>();
        String choice;
        UserInput uiObj;

        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(new ModificationWatcher(), 0, 3, TimeUnit.SECONDS);

        do {
            uiObj = InputAcceptor.enterInput();
            result.clear();
            synchronized (Utility.tshirtsInfo) {
                for (HashSet<Tshirt> set : tshirtsInfo.values()) {
                    for (Tshirt t : set) {
                        if (t.getColor().equalsIgnoreCase(uiObj.getColor())
                                && t.getSize().equalsIgnoreCase(uiObj.getSize())
                                && t.getGenderRecommendation().equalsIgnoreCase(uiObj.getGenderRecommendation())
                          
                                && t.isAvailability())
                            result.add(t);
                    }
                }
            }
            if (uiObj.getOutputPreference() == 1)
                Collections.sort(result, new TshirtPriceComparator());
            else
                Collections.sort(result,new TshirtRatingComparator());
            System.out.println("\nResult:");
            for (Tshirt t : result) {
                System.out.println(t);
            }
            System.out.print("\nWant to Exit (Enter Y/N): ");
            while (!((choice = br.readLine()).equalsIgnoreCase("y") || choice.equalsIgnoreCase("n")))
                System.out.print("I could not Understand Enter Again:");
        } while (choice.equalsIgnoreCase("n"));
        service.shutdown();
    }

}
